from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.shortcuts import render
from core.forms import ContatoForm, UsuarioForm
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView, TemplateView
from core.models import Evento, Usuario
from core.forms import EventoStatus
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import permission_required, user_passes_test
from django.utils.decorators import method_decorator

class LoginMixin(object):

    @classmethod
    def as_view(cls, **kwargs):
        view = super(LoginMixin, cls).as_view(**kwargs)
        return login_required(view)


class EventoList(LoginMixin, ListView):
    model = Evento
    paginate_by = 2


class EventoDetail(LoginMixin, DetailView):
    model = Evento

class HomeView(LoginMixin, TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['eventos_count'] = Evento.objects.count()
        return context


def grupo_eventos(usuario):
    if usuario:
        return usuario.groups.filter(name='Assessoria de Imprensa').exists()
    return False

class EventoCreate(LoginMixin, SuccessMessageMixin, CreateView):
    model = Evento
    success_url = '/eventos/'
    success_message = '%(titulo)s criado com sucesso'

    @method_decorator(user_passes_test(grupo_eventos))
    def dispatch(self, *args, **kwargs):
        return super(EventoCreate, self).dispatch(*args, **kwargs)


class EventoUpdate(LoginMixin, SuccessMessageMixin, UpdateView):
    model = Evento
    success_url = '/eventos/'
    success_message = '%(titulo)s alterado com sucesso'

    @method_decorator(permission_required('core.change_evento'))
    def dispatch(self, *args, **kwargs):
        return super(EventoUpdate, self).dispatch(*args, **kwargs)


class EventoDelete(DeleteView):
    model = Evento
    success_url = '/eventos/'


class UsuarioCreate(CreateView):
    model = Usuario
    success_url = '/'
    form_class = UsuarioForm

    @method_decorator(user_passes_test(lambda u: u.is_superuser))
    def dispatch(self, *args, **kwargs):
        return super(UsuarioCreate, self).dispatch(*args, **kwargs)


@login_required
def contato(request):

    form = ContatoForm(request.POST or None)

    if form.is_valid() and request.method == 'POST':
        nome = form.cleaned_data['nome']
        email = form.cleaned_data['email']
        mensagem = form.cleaned_data['mensagem']
        print nome, email
        send_mail('Mensagem enviada por %s' % nome, mensagem, email,
                  ['gabriel@128bits.cc', 'gabrielfreitas07@gmail.com'] )

    return render(request, 'contato.html', {'form':form})