# coding: utf-8
from django.contrib import admin
from core.models import Evento, Participante, Usuario
from django.db import models
from redactor.widgets import RedactorEditor
import reversion

class ParticipanteInline(admin.TabularInline):
    model = Participante


class EventoAdmin(reversion.VersionAdmin):
    list_display = ('titulo', 'data_inicio', 'data_fim','status')
    list_display_links = ('titulo', 'data_fim', )
    list_editable = ('status', 'data_inicio')
    list_filter = ['status', 'data_inicio', 'data_fim', 'inscricao']
    search_fields = ['titulo', 'conteudo', 'local']
    save_as = True
    # fieldsets = [
    #     ('Informações Gerais', {'fields': ['titulo', 'conteudo', 'local', 'site']}),
    #     ('Inscrições', {'fields': ['inscricao', 'site_inscricao',
    #                                'local_inscricao', 'data_inicio_inscricao',
    #                                'data_fim_inscricao'],
    #                     'classes': ['collapse']}),
    # ]
    inlines = [ParticipanteInline]
    save_as = True
    actions = ['ativar_status', 'desativar_status']

    formfield_overrides = {
        models.TextField: {'widget': RedactorEditor()},
    }

    def ativar_status(self, request, queryset):
        queryset.update(status=True)
    ativar_status.short_description = 'Ativar todos os status'

    def desativar_status(self, request, queryset):
        queryset.update(status=False)


class UsuarioAdmin(reversion.VersionAdmin):
    save_as = True
    pass

admin.site.register(Evento, EventoAdmin)
admin.site.register(Participante)
admin.site.register(Usuario, UsuarioAdmin)
