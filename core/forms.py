from django import forms
from core.models import Evento, Usuario

class ContatoForm(forms.Form):
    nome = forms.CharField(max_length=50)
    email = forms.EmailField()
    mensagem = forms.CharField(widget=forms.Textarea)


class EventoStatus(forms.ModelForm):


    class Meta:
        model = Evento
        fields = ('status', )


class UsuarioForm(forms.ModelForm):

    password = forms.CharField(widget=forms.PasswordInput)

    def save(self, commit=True):
        user = super(UsuarioForm, self).save(commit=False)
        user.set_password(self.data['password'])
        user.is_staff = True
        user.is_active = True
        if commit:
            user.save()
        return user

    class Meta:
        model = Usuario
        fields = ('matricula', 'email', 'password', 'username', 'first_name')

