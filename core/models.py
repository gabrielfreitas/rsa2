# coding: utf-8
from datetime import datetime
from django.conf import settings
from django.db import models
from django.contrib.auth.models import User, AbstractUser, BaseUserManager
import reversion

STATUS = (
    ('Ativo', 'Ativo'),
    ('Inativo', 'Inativo'),
)

@reversion.register
class CommonModel(models.Model):
    data_criacao = models.DateTimeField(default=datetime.now, auto_now_add=True)
    data_atualizacao = models.DateTimeField(default=datetime.now, auto_now=True)
    status = models.CharField(max_length=20, default='Ativo', choices=STATUS)

    class Meta:
        abstract = True

@reversion.register
class Evento(CommonModel):

    titulo = models.CharField(max_length=100)
    conteudo = models.TextField()
    data_inicio = models.DateField()
    data_fim = models.DateField()
    inscricao = models.BooleanField(default=True,
                                    help_text='O evento vai ter inscrição?')

    data_inicio_inscricao = models.DateField(blank=True, null=True)
    data_fim_inscricao = models.DateField(blank=True, null=True)

    local = models.CharField(max_length=100, default='IFPI')
    site = models.URLField(blank=True, null= True)

    local_inscricao = models.CharField(max_length=100, blank=True, null= True)
    site_inscricao = models.URLField(blank=True, null= True)


    # user = models.ForeignKey(User, blank= True, null=True)

    def __unicode__(self):
        return self.titulo

    class Meta:
        ordering = ['-data_criacao', '-data_atualizacao']
        verbose_name = 'Evento'
        verbose_name_plural = 'Eventos'


class Participante(CommonModel):
    nome = models.CharField(max_length=100)
    email = models.EmailField()
    evento = models.ForeignKey(Evento)


class Blog(CommonModel):
    nome = models.CharField(max_length=50)


class CustomUserManager(BaseUserManager):

    def create_user(self, matricula, email, password=None):
        if not matricula:
            raise ValueError("Matricula nao definida")
        user = self.model(matricula=matricula, email=email)
        user.set_password(password)
        user.is_staff = True
        user.save(using=self.db)
        return user

    def create_superuser(self, matricula, email, password):
        user = self.create_user(matricula, email, password=password)
        user.is_superuser = True
        user.save(using=self.db)
        return user


class Usuario(AbstractUser):

    matricula = models.CharField(max_length=10, unique=True)

    objects = CustomUserManager()

    USERNAME_FIELD = 'matricula'