# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import django.utils.timezone
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True, max_length=30, verbose_name='username', validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Enter a valid username.', 'invalid')])),
                ('first_name', models.CharField(max_length=30, verbose_name='first name', blank=True)),
                ('last_name', models.CharField(max_length=30, verbose_name='last name', blank=True)),
                ('email', models.EmailField(max_length=75, verbose_name='email address', blank=True)),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
                ('matricula', models.CharField(unique=True, max_length=10)),
                ('groups', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of his/her group.', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions')),
            ],
            options={
                'abstract': False,
                'verbose_name': 'user',
                'verbose_name_plural': 'users',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Blog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data_criacao', models.DateTimeField(default=datetime.datetime.now, auto_now_add=True)),
                ('data_atualizacao', models.DateTimeField(default=datetime.datetime.now, auto_now=True)),
                ('status', models.CharField(default=b'Ativo', max_length=20, choices=[(b'Ativo', b'Ativo'), (b'Inativo', b'Inativo')])),
                ('nome', models.CharField(max_length=50)),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Evento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data_criacao', models.DateTimeField(default=datetime.datetime.now, auto_now_add=True)),
                ('data_atualizacao', models.DateTimeField(default=datetime.datetime.now, auto_now=True)),
                ('status', models.CharField(default=b'Ativo', max_length=20, choices=[(b'Ativo', b'Ativo'), (b'Inativo', b'Inativo')])),
                ('titulo', models.CharField(max_length=100)),
                ('conteudo', models.TextField()),
                ('data_inicio', models.DateField()),
                ('data_fim', models.DateField()),
                ('inscricao', models.BooleanField(default=True, help_text=b'O evento vai ter inscri\xc3\xa7\xc3\xa3o?')),
                ('data_inicio_inscricao', models.DateField(null=True, blank=True)),
                ('data_fim_inscricao', models.DateField(null=True, blank=True)),
                ('local', models.CharField(default=b'IFPI', max_length=100)),
                ('site', models.URLField(null=True, blank=True)),
                ('local_inscricao', models.CharField(max_length=100, null=True, blank=True)),
                ('site_inscricao', models.URLField(null=True, blank=True)),
            ],
            options={
                'ordering': ['-data_criacao', '-data_atualizacao'],
                'verbose_name': 'Evento',
                'verbose_name_plural': 'Eventos',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Participante',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('data_criacao', models.DateTimeField(default=datetime.datetime.now, auto_now_add=True)),
                ('data_atualizacao', models.DateTimeField(default=datetime.datetime.now, auto_now=True)),
                ('status', models.CharField(default=b'Ativo', max_length=20, choices=[(b'Ativo', b'Ativo'), (b'Inativo', b'Inativo')])),
                ('nome', models.CharField(max_length=100)),
                ('email', models.EmailField(max_length=75)),
                ('evento', models.ForeignKey(to='core.Evento')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
    ]
