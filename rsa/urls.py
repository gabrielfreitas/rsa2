from django.conf.urls import patterns, include, url
from django.contrib import admin
from core.views import *


urlpatterns = patterns('',
    # Examples:
    url(r'^$', HomeView.as_view(), name='home'),
    url(r'^contato/$', 'core.views.contato', name='contato'),
    url(r'^eventos/$', EventoList.as_view(), name='evento_list'),
    url(r'^eventos/add/$', EventoCreate.as_view(), name='evento_add'),
    url(r'^eventos/edit/(?P<pk>\d+)/$', EventoUpdate.as_view(), name='evento_update'),
    url(r'^eventos/(?P<pk>\d+)/$', EventoDetail.as_view(), name='evento_detail'),
    url(r'^eventos/delete/(?P<pk>\d+)/$', EventoDelete.as_view(), name='evento_del'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^criar-conta/$', UsuarioCreate.as_view(), name='usuario_add'),

    url(r'auth/', include('django.contrib.auth.urls', namespace='auth')),

    url(r'^admin/', include(admin.site.urls)),
)
