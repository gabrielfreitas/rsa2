from core.models import Evento, Usuario


def contadores(request):
    eventos_count = Evento.objects.count()
    eventos_ativos = Evento.objects.filter(status=True).count()
    usuarios_count = Usuario.objects.count()
    return {'eventos_count': eventos_count,
            'eventos_ativos': eventos_ativos,
            'usuarios_count': usuarios_count}